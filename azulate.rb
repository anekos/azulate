#!/usr/bin/ruby
# vim: set fileencoding=utf-8 :

require 'dotenv'
require 'uri'
require 'time'
require 'pp'
require 'rexml/document'
require 'shellwords'
require 'net/http'


class String
  def clean
    # コメントっぽいの
    self.gsub(%r[\s*(#|//|/\*)], '')
  end

  def guess_languages
    jp_size = self.scan(/\p{Hiragana}|\p{Katakana}|[ー－]|[一-龠々]/).size
    en_size = self.scan(/[a-zA-Z]/).size

    if jp_size < en_size
      ['en', 'ja']
    else
      ['ja', 'en']
    end
  end
end


class App
  def translate(text)
    text = text.split(/\n/).map(&:clean).join(' ').strip

    raise 'Empty text' if text.empty?

    result = fetch_cache(text) and return result

    from, to = text.guess_languages
    result = call_api(text, from, to)
    cache(text, result)
    result
  end

  def start_shell
    require 'readline' # https://docs.ruby-lang.org/ja/latest/class/Readline.html
    begin
      while line = Readline.readline('> ', true) or break
        puts(translate(line)) unless line.empty?
      end
    rescue Interrupt
    end
    puts
  end

  def call_api(text, from, to)
    key = ENV['TRANSLATOR_TEXT_API_KEY1']
    uri = URI('https://api.microsofttranslator.com/V2/Http.svc/Translate')

    # `contentType` は API に入力する文字列のタイプ
    merged = {'text' => text, 'from' => from, 'to' => to, 'contentType' => 'text/plain'}

    uri.query = URI.encode_www_form(merged)

    request = Net::HTTP::Get.new(uri.request_uri)
    request['Content-Type'] = 'multipart/form-data'
    request['Ocp-Apim-Subscription-Key'] = key
    request.body = "{body}"

    response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
      http.request(request)
    end

    body = REXML::Document.new(response.body)

    raise body.to_s unless Net::HTTPOK === response

    body.elements['string'].text
  end

  def fetch_cache(text)
    result = `hugo azulate get #{text.shellescape}`
    $?.success? ? result : nil
  end

  def cache(text, translated)
    `hugo azulate set #{text.shellescape} #{translated.shellescape}`
  end
end


if __FILE__ == $0
  Dotenv.load

  app = App.new

  text =
    if File.pipe?(STDIN)
      ARGF.read
    else
      if ARGV.empty?
        app.start_shell
        exit
      end

      ARGV.join(' ')
    end

  puts(app.translate(text))
end
